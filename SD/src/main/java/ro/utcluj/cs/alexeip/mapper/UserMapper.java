package ro.utcluj.cs.alexeip.mapper;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.User;
import ro.utcluj.cs.alexeip.factory.UserDTOFactory;
import ro.utcluj.cs.alexeip.factory.UserFactory;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

@Component
@AllArgsConstructor
public class UserMapper {
    private UserFactory userFactory;
    private UserDTOFactory userDTOFactory;

    public User toUser(UserDTO userDTO) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException,
            InstantiationException, IllegalAccessException, ParseException
    {
        return userFactory.toUser(userDTO);
    }

    public UserDTO toUserDTO(User user) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException,
            InstantiationException, IllegalAccessException
    {
        return userDTOFactory.toUserDTO(user);
    }
}
