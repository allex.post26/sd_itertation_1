package ro.utcluj.cs.alexeip.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcluj.cs.alexeip.entity.MedicalRecord;

public interface MedicalRecordRepository extends CrudRepository<MedicalRecord, Integer> {
}
