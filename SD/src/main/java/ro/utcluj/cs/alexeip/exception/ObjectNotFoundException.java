package ro.utcluj.cs.alexeip.exception;

public class ObjectNotFoundException extends Exception{
    @Override
    public String toString() {return super.toString() + "\n Not found in the database!"; }
}
