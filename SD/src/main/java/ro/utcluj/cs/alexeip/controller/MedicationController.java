package ro.utcluj.cs.alexeip.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ro.utcluj.cs.alexeip.entity.User;
import ro.utcluj.cs.alexeip.exception.DatabasePersistenceException;
import ro.utcluj.cs.alexeip.exception.ObjectNotFoundException;
import ro.utcluj.cs.alexeip.service.MedicationService;

import java.util.List;

@RestController @AllArgsConstructor
public class MedicationController {
    private MedicationService medicationService;

    @GetMapping("/medications")
    public ResponseEntity<List> retrieveMedications() {
        try {
            return new ResponseEntity(medicationService.findAll(), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            return new ResponseEntity(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @PostMapping("/medications")
//    public ResponseEntity<User> createUser(@RequestBody MedicationDTO medicationDTO) {
//        try {
//            medicationService.save(medicationDTO);
//        } catch (DatabasePersistenceException e) {
//            return new ResponseEntity(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//        return new ResponseEntity("User created", HttpStatus.OK);
//    }
}
