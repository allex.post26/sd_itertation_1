package ro.utcluj.cs.alexeip.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcluj.cs.alexeip.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {
}
