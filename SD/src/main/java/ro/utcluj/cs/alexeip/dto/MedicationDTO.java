package ro.utcluj.cs.alexeip.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data @NoArgsConstructor @AllArgsConstructor
public class MedicationDTO {
    private String ID;
    private String name;
    private String dosage;
    private ArrayList<String> sideEffects;
}
