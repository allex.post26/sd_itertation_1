package ro.utcluj.cs.alexeip.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcluj.cs.alexeip.entity.TreatmentPlan;

public interface TreatmentPlanRepository extends CrudRepository<TreatmentPlan, Integer> {
}
