package ro.utcluj.cs.alexeip.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcluj.cs.alexeip.entity.SideEffect;

public interface SideEffectRepository extends CrudRepository<SideEffect, Integer> {
}
