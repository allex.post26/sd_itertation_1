package ro.utcluj.cs.alexeip.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.User;
import ro.utcluj.cs.alexeip.exception.DatabasePersistenceException;
import ro.utcluj.cs.alexeip.exception.ObjectNotFoundException;
import ro.utcluj.cs.alexeip.mapper.UserMapper;
import ro.utcluj.cs.alexeip.repository.UserRepository;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service @AllArgsConstructor
public class UserService {
    private UserRepository userRepository;
    private UserMapper userMapper;

    public void save(UserDTO userDTO) throws DatabasePersistenceException {
        try {
            userRepository.save(userMapper.toUser(userDTO));
        } catch (ClassNotFoundException | NoSuchMethodException |
                InvocationTargetException | InstantiationException |
                IllegalAccessException | ParseException e
        ) {
            throw new DatabasePersistenceException();
        }
    }

    public UserDTO findByID(String sID) throws ObjectNotFoundException, DatabasePersistenceException {
        User user = userRepository.findById(Integer.parseInt(sID)).orElse(null);
        if (user == null) throw new ObjectNotFoundException();
        try {
            return userMapper.toUserDTO(user);
        } catch (ClassNotFoundException |NoSuchMethodException |
                InvocationTargetException | InstantiationException |
                IllegalAccessException e) {
            throw new DatabasePersistenceException();
        }
    }

    public List<UserDTO> findAll() throws ObjectNotFoundException {
        Iterable<User> iUsers = userRepository.findAll();
        ArrayList<UserDTO> userDTOS = new ArrayList<>();
        ArrayList<User> users = new ArrayList<>();
        iUsers.forEach(users::add);

        try {
            for (User user: users) {
                userDTOS.add(userMapper.toUserDTO(user));
            }
        } catch (Exception e) {
            throw new ObjectNotFoundException();
        }

        return userDTOS;
    }

    public void update(String sID, UserDTO userDTO) throws DatabasePersistenceException {
        try {
            userDTO.setID(sID);
            userRepository.save(userMapper.toUser(userDTO));
        } catch (ClassNotFoundException | NoSuchMethodException |
                InvocationTargetException | InstantiationException |
                IllegalAccessException | ParseException e
        ) {
            throw new DatabasePersistenceException();
        }
    }

    public void deleteById(String sID) throws DatabasePersistenceException {
        try {
            userRepository.deleteById(Integer.parseInt(sID));
        } catch (Exception e) {
            throw new DatabasePersistenceException();
        }
    }
}
