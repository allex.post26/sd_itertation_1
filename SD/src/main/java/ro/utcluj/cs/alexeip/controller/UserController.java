package ro.utcluj.cs.alexeip.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.User;
import ro.utcluj.cs.alexeip.exception.DatabasePersistenceException;
import ro.utcluj.cs.alexeip.exception.ObjectNotFoundException;
import ro.utcluj.cs.alexeip.service.UserService;

import java.util.List;

@RestController
@AllArgsConstructor
public class UserController {
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List> retrieveUsers() {
        try {
            return new ResponseEntity(userService.findAll(), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            return new ResponseEntity(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<User> createUser(@RequestBody UserDTO userDTO) {
        try {
            userService.save(userDTO);
        } catch (DatabasePersistenceException e) {
            return new ResponseEntity(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity("User created", HttpStatus.OK);
    }

    @GetMapping("/user")
    public ResponseEntity<User> retrieveUser(@RequestParam String id) {
        try {
            return new ResponseEntity(userService.findByID(id), HttpStatus.OK);
        } catch (ObjectNotFoundException | DatabasePersistenceException e) {
            return new ResponseEntity(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity updateUser(@RequestBody UserDTO userDTO, @PathVariable String id) {
        try {
            userService.update(id, userDTO);
            return new ResponseEntity("User updated", HttpStatus.OK);
        } catch (DatabasePersistenceException e) {
            return new ResponseEntity("Update of user " + id + "failed.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable String id) {
        try {
            userService.deleteById(id);
            return new ResponseEntity("User deleted successfully!", HttpStatus.OK);
        } catch (DatabasePersistenceException e) {
            return new ResponseEntity("Delete of user " + id + " failed.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
