package ro.utcluj.cs.alexeip.builder;

import lombok.Data;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.dto.UserDTO;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component @Data
public class UserDTOBuilder {
    private UserDTO userDTO;

    public void build() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.userDTO = new UserDTO();
    }

    public void addID(int ID) {userDTO.setID(Integer.toString(ID)); }
    public void addName(String name) { userDTO.setName(name); }
    public void addGender(String gender) { userDTO.setGender(gender); }
    public void addAddress(String address) { userDTO.setAddress(address); }
    public void addType(String type) { userDTO.setType(type); }
    public void addBirthDate(Date birthDate) {
        String sBirthDate = new SimpleDateFormat("dd/MM/yyyy").format(birthDate);
        userDTO.setBirthDate(sBirthDate);
    }
}
