package ro.utcluj.cs.alexeip.repository;

import org.springframework.data.repository.CrudRepository;
import ro.utcluj.cs.alexeip.entity.Medication;

public interface MedicationRepository extends CrudRepository<Medication, Integer> {
}
