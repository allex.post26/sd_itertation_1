package ro.utcluj.cs.alexeip.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
@Data @NoArgsConstructor @AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int ID;
    @Column(name = "name",nullable = false)
    private String name;
    @Column(name = "birthdate", nullable = false)
    private Date birthDate;
    @Column(name="gender", nullable = false)
    private String gender;
    @Column(name="address", nullable = false)
    private String address;
    @Column(name="type", nullable = false)
    private String type;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private MedicalRecord medicalRecord;
    @JoinTable(name = "caregiver_patients")
    @OneToMany(fetch = FetchType.LAZY)
    private List<User> patients;
}


