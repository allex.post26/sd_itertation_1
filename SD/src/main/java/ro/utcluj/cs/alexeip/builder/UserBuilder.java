package ro.utcluj.cs.alexeip.builder;

import lombok.Data;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.entity.User;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component @Data
public class UserBuilder {
    private User user;

    public void build() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.user = new User();
    }

    public void addID(String sID) {
        if (sID == null) return;
        int ID = Integer.parseInt(sID);
        if (ID == 0) return;
        user.setID(ID);
    }
    public void addName(String name) { user.setName(name); }
    public void addGender(String gender) { user.setGender(gender); }
    public void addAddress(String address) { user.setAddress(address); }
    public void addType(String type) { user.setType(type); }
    public void addBirthDate(String sBirthDate) throws ParseException {
        Date birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(sBirthDate);
        user.setBirthDate(birthDate);
    }
}
