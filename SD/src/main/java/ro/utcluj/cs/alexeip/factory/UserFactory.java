package ro.utcluj.cs.alexeip.factory;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.builder.UserBuilder;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.User;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

@Component @AllArgsConstructor
public class UserFactory {
    private UserBuilder userBuilder;

    public User toUser(UserDTO userDTO) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException,
            InstantiationException, IllegalAccessException, ParseException
    {
        userBuilder.build();
        userBuilder.addID(userDTO.getID());
        userBuilder.addName(userDTO.getName());
        userBuilder.addBirthDate(userDTO.getBirthDate());
        userBuilder.addGender(userDTO.getGender());
        userBuilder.addAddress(userDTO.getAddress());
        userBuilder.addType(userDTO.getType());

        return userBuilder.getUser();
    }
}
