package ro.utcluj.cs.alexeip.builder;

import lombok.Data;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.entity.Medication;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

@Component @Data
public class MedicationBuilder {
    private Medication medication;

    public void build() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException, NumberFormatException {
        this.medication = new Medication();
    }

    public void addID(String sID) {
        if (sID == null) return;
        int ID = Integer.parseInt(sID);
        if (ID == 0) return;
        medication.setID(ID);
    }
    public void addName(String name) { medication.setName(name); }
    public void addDosage(String dosage) {medication.setDosage(Double.parseDouble(dosage));}
    public void addSideEffects(ArrayList<String> sSideEffects) {
        //TODO implement sideEffectsMapper -> NEXT THING TO DO
    }
}
