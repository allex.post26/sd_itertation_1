package ro.utcluj.cs.alexeip.mapper;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.dto.MedicationDTO;
import ro.utcluj.cs.alexeip.entity.Medication;
import ro.utcluj.cs.alexeip.entity.User;
import ro.utcluj.cs.alexeip.factory.MedicationDTOFactory;
import ro.utcluj.cs.alexeip.factory.MedicationFactory;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

@Component
@AllArgsConstructor
public class MedicationMapper {
    private MedicationFactory medicationFactory;
    private MedicationDTOFactory medicationDTOfactory;

    public Medication toMedication(MedicationDTO medicationDTO) throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException, IllegalAccessException, ParseException
    {
        return medicationFactory.toMedication(medicationDTO);
    }

    public MedicationDTO toMedicationDTO(Medication medication) throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException, IllegalAccessException
    {
        return medicationDTOfactory.toMedicationDTO(medication);
    }
}
