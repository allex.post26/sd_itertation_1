package ro.utcluj.cs.alexeip.builder;

import lombok.Data;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.dto.MedicationDTO;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.SideEffect;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component @Data
public class MedicationDTOBuilder {
    private MedicationDTO medicationDTO;

    public void build() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException
    {
        this.medicationDTO = new MedicationDTO();
    }

    public void addID(int ID) {medicationDTO.setID(Integer.toString(ID)); }
    public void addName(String name) { medicationDTO.setName(name); }
    public void addDosage(double dosage) { medicationDTO.setDosage(String.valueOf(dosage)); }
    public void addSideEffects(List<SideEffect> sideEffects) {
        //TODO implement the sideEffectDTOMapper -> Second thing to do
    }
}
