package ro.utcluj.cs.alexeip.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Embeddable
@Entity
@Table(name = "medications")
@Data @NoArgsConstructor @AllArgsConstructor
public class Medication implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int ID;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private double dosage;
    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinTable(
            name = "medication_side_effects",
            joinColumns  = {
                    @JoinColumn(name = "medication_id", referencedColumnName = "id"),
                    @JoinColumn(name = "medication_name", referencedColumnName = "name")},
            inverseJoinColumns = {
                    @JoinColumn(name = "side_effect_id", referencedColumnName = "id"),
                    @JoinColumn(name = "side_effect_name", referencedColumnName = "name"),
            }
    )
    private List<SideEffect> side_effects;
}
