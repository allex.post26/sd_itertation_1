package ro.utcluj.cs.alexeip.factory;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.builder.UserDTOBuilder;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.User;

import java.lang.reflect.InvocationTargetException;

@Component
@Data
@AllArgsConstructor
public class UserDTOFactory {
    private UserDTOBuilder userDTOBuilder;
    public UserDTO toUserDTO(User user) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        userDTOBuilder.build();
        userDTOBuilder.addID(user.getID());
        userDTOBuilder.addName(user.getName());
        userDTOBuilder.addBirthDate(user.getBirthDate());
        userDTOBuilder.addGender(user.getGender());
        userDTOBuilder.addAddress(user.getAddress());
        userDTOBuilder.addType(user.getType());

        return userDTOBuilder.getUserDTO();
    }
}
