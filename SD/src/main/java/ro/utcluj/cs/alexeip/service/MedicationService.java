package ro.utcluj.cs.alexeip.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ro.utcluj.cs.alexeip.dto.MedicationDTO;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.Medication;
import ro.utcluj.cs.alexeip.exception.ObjectNotFoundException;
import ro.utcluj.cs.alexeip.mapper.MedicationMapper;
import ro.utcluj.cs.alexeip.repository.MedicationRepository;

import java.util.ArrayList;
import java.util.List;

@Service @AllArgsConstructor
public class MedicationService {
    private MedicationRepository medicationRepository;
    private MedicationMapper medicationMapper;

    public List<MedicationDTO> findAll() throws ObjectNotFoundException {
        Iterable<Medication> iMedications = medicationRepository.findAll();
        ArrayList<MedicationDTO> medicationDTOS = new ArrayList<>();
        ArrayList<Medication> medications = new ArrayList<>();
        iMedications.forEach(medications::add);

        try {
            for (Medication medication: medications) {
                medicationDTOS.add(medicationMapper.toMedicationDTO(medication));
            }
        } catch (Exception e) {
            throw new ObjectNotFoundException();
        }

        return medicationDTOS;
    }
}
