package ro.utcluj.cs.alexeip.factory;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.builder.MedicationDTOBuilder;
import ro.utcluj.cs.alexeip.dto.MedicationDTO;
import ro.utcluj.cs.alexeip.dto.UserDTO;
import ro.utcluj.cs.alexeip.entity.Medication;
import ro.utcluj.cs.alexeip.entity.User;

import java.lang.reflect.InvocationTargetException;

@Component
@Data @AllArgsConstructor
public class MedicationDTOFactory {
    private MedicationDTOBuilder medicationDTOBuilder;
    public MedicationDTO toMedicationDTO(Medication medication) throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException, IllegalAccessException
    {
        medicationDTOBuilder.build();
        medicationDTOBuilder.addID(medication.getID());
        medicationDTOBuilder.addName(medication.getName());
        medicationDTOBuilder.addDosage(medication.getDosage());
        if (medication.getSide_effects() != null && !medication.getSide_effects().isEmpty()) {
            medicationDTOBuilder.addSideEffects(medication.getSide_effects());
        }

        return medicationDTOBuilder.getMedicationDTO();
    }
}
