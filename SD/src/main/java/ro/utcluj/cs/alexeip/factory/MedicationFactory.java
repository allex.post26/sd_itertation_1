package ro.utcluj.cs.alexeip.factory;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ro.utcluj.cs.alexeip.builder.MedicationBuilder;
import ro.utcluj.cs.alexeip.dto.MedicationDTO;
import ro.utcluj.cs.alexeip.entity.Medication;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;

@Component @AllArgsConstructor
public class MedicationFactory {
    private MedicationBuilder medicationBuilder;

    public Medication toMedication(MedicationDTO medicationDTO) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException,
            InstantiationException, IllegalAccessException, ParseException
    {
        medicationBuilder.build();
        medicationBuilder.addID(medicationDTO.getID());
        medicationBuilder.addName(medicationDTO.getName());
        medicationBuilder.addDosage(medicationDTO.getDosage());
        if (medicationDTO.getSideEffects() != null && !medicationDTO.getSideEffects().isEmpty()) {
            medicationBuilder.addSideEffects(medicationDTO.getSideEffects());
        }

        return medicationBuilder.getMedication();
    }
}
