package ro.utcluj.cs.alexeip.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class UserDTO {
    private String ID;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private String type;
}
